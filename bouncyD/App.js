/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import BouncyDrawer from 'react-native-bouncy-drawer'

export default class App extends Component<Props> {
  renderContent = () => (
    <View style={{ backgroundColor: '#3F3C4C' ,flex:1}}>
      <View style={{ flex:1}}>
        <Text>Osman</Text>
      </View>
    </View>
  )
  render() {
  return (
    <View>
    <BouncyDrawer
      willOpen={() => console.log('will open')}
      didOpen={() => console.log('did open')}
      willClose={() => console.log('will close')}
      didClose={() => console.log('did close')}
      title="Activity"
      titleStyle={{ color: '#fff', fontSize: 20, marginLeft: -3 }}
      closedHeaderStyle={{ backgroundColor: '#3F3C4C' }}
      defaultOpenButtonIconColor="#fff"
      defaultCloseButtonIconColor="#fff"
      renderContent={this.renderContent}
      openedHeaderStyle={{ backgroundColor: '#3F3C4C' }}
      />
      </View>
  );
}
}
